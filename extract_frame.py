import numpy as np
import cv2
import os, glob
import json
import collections
import argparse
import tqdm

FRAMES_PER_CLIP = 16
CROP_SIZE = 112
SAMPLE_RATE = 10

class VideoSpliter():
    def __init__(self, type,
                 annotation_prefix,
                 video_prefix,
                 frame_prefix):
        '''
        :param type: train/val_1/val_2
        :param annotation_prefix: prefix of annotation file
        :param video_prefix : prefix of video file
        :param frame_prefix : prefix of frames
        '''
        self.type = type
        self.video_prefix = video_prefix
        self.frame_prefix = os.path.join(frame_prefix, type)
        self.json_path = os.path.join(annotation_prefix, type + '.json')
        self.data = collections.OrderedDict(json.load(file(self.json_path))).items()

    def save_frame(self, vid):
        # video frame
        cap = cv2.VideoCapture(os.path.join(self.video_prefix, vid) + '.mp4')

        # save directory
        save_dir = os.path.join(self.frame_prefix, vid)
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        sample_rate = 1 if frame_count < FRAMES_PER_CLIP*SAMPLE_RATE else SAMPLE_RATE

        for ix in range(frame_count):
            ret, frame = cap.read()
            if not ret: break

            if ix % sample_rate == 0:
                height, width, channel = frame.shape
                if (width > height):
                    scale = float(CROP_SIZE) / float(height)
                    frame = cv2.resize(frame, (int(width * scale + 1), CROP_SIZE)).astype(np.float32)
                else:
                    scale = float(CROP_SIZE) / float(width)
                    frame = cv2.resize(frame, (CROP_SIZE, int(height * scale + 1))).astype(np.float32)

                crop_x = int((frame.shape[1] - CROP_SIZE) / 2)
                crop_y = int((frame.shape[0] - CROP_SIZE) / 2)

                cropped = frame[crop_y:crop_y + CROP_SIZE, crop_x:crop_x + CROP_SIZE, :]

                # save path for each frame
                save_path = os.path.join(save_dir, str(ix/sample_rate) + '.jpg')
                cv2.imwrite(save_path, cropped)

    def run(self):
        for vid, anno in tqdm.tqdm(self.data):
            self.save_frame(vid)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--which', help='which dataset type')
    parser.add_argument('--annotation_prefix', help='prefix of annotation', default='./Dense_VTT/annotation')
    parser.add_argument('--video_prefix', help='prefix of video', default='/mnt/hosang/Dense_VTT/video')
    parser.add_argument('--frame_prefix', help='prefix of frame', default='/mnt/hosang/Dense_VTT/frames')
    args = parser.parse_args()

    VideoSpliter(type=args.which,
                 annotation_prefix=args.annotation_prefix,
                 video_prefix=args.video_prefix,
                 frame_prefix=args.frame_prefix).run()