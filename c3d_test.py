import tensorflow as tf
import c3d_wrapper
import matplotlib.pyplot as plt
import os

os.environ["CUDA_VISIBLE_DEVICES"]="1"

net = c3d_wrapper.C3DNet(
    pretrained_model_path='../pretrained/C3D/conv3d_deepnetA_sport1m_iter_1900000_TF.model', trainable=False)
tf_video_clip = tf.placeholder(tf.float32,
                               [1, None, 112, 112, 3],
                               name='tf_video_clip')  # (batch,num_frames,112,112,3)
tf_output = net(inputs=tf_video_clip)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

with open('labels.txt', 'r') as f:
    labels = [line.strip() for line in f.readlines()]
print('Total labels: {}'.format(len(labels)))

import cv2
import numpy as np

mean_val = np.load('train01_16_128_171_mean.npy').transpose(1,2,3,0)
cap = cv2.VideoCapture('hPcR6MQ5dJo.mp4')
vid = []
while True:
    ret, img = cap.read()
    if not ret:
        break
    vid.append(cv2.resize(img, (171, 128)))
vid = np.array(vid, dtype=np.float32)

X = vid[0:16]-mean_val
X = X[:, 8:120, 30:142, :]

output = sess.run(tf_output, feed_dict={tf_video_clip:[X]})
#plt.plot(output[0]); plt.show()

print('Position of maximum probability: {}'.format(output[0].argmax()))
print('Maximum probability: {:.5f}'.format(max(output[0])))
print('Corresponding label: {}'.format(labels[output[0].argmax()]))

# sort top five predictions from softmax output
top_inds = output[0].argsort()[::-1][:5]  # reverse sort and take five largest items
print('\nTop 5 probabilities and labels:')

for i in top_inds:
    print('{:.5f} {}'.format(output[0][i], labels[i]))