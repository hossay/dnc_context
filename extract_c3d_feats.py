import tensorflow as tf
import numpy as np
import sys, os

import tqdm
import c3d_wrapper
from data_utils import FrameBatcher
from config import FLAGS

# static vars
BATCH_SIZE = 1
FRAMES_PER_CLIP = 16
SAMPLE_RATE = 10
CROP_SIZE = 112

os.environ["CUDA_VISIBLE_DEVICES"]="1"

def llprint(message):
    sys.stdout.write(message)
    sys.stdout.flush()

def get_feats_sequence(net, tf_video_clip):
    '''
    Extract c3d features from tf_video_clip
    :param net: c3dnet instance
    :param tf_video_clip: placeholder for input video clip at specific event index
                          shape = (batch_size, n_frames, height, width, 3)
    :return: sequence of extracted feature from given video clip
    '''
    def body(i, ta):
        c3d_feats = net(inputs=tf_video_clip[:, i*FRAMES_PER_CLIP:(i+1)*FRAMES_PER_CLIP])
        ta = ta.write(i,c3d_feats)
        return i+1, ta

    i = tf.constant(0)
    out_length = tf.maximum(tf.shape(tf_video_clip)[1]/FRAMES_PER_CLIP, 1)

    ta = tf.TensorArray(dtype=tf.float32, size=out_length)
    _, feats_sequence = tf.while_loop(cond=lambda i,*_ : i<out_length,
                                      body=body,
                                      loop_vars=(i,ta))

    return tf.transpose(feats_sequence.stack(), (1,0,2))


def build_graph(ph):
    # build C3D network
    net = c3d_wrapper.C3DNet(
        pretrained_model_path='../pretrained/C3D/conv3d_deepnetA_sport1m_iter_1900000_TF.model', trainable=False)

    # placeholders
    video_clip = ph['video_clip']

    # c3d feats_sequence
    feats_sequence = get_feats_sequence(net, video_clip)

    return dict(feats_sequence=feats_sequence,
                net=net)


def step(sess, g, ph, batcher, dir):
    feed_data = batcher.prepare_feed_data(session=sess,
                                          feats_sequence=g['feats_sequence'],
                                          tf_video_clip=ph['video_clip'])

    _input_data_list = feed_data

    vid, data_list = _input_data_list[0][0], _input_data_list[1:]
    _dir = os.path.join(dir, vid)
    if not os.path.exists(_dir):
        os.makedirs(_dir)

    for i in range(len(data_list)):
        file_path = os.path.join(_dir, 'event-{}.npy'.format(i))
        np.save(file_path, data_list[i])

def save_feats(batcher, dir):
    ph = dict(video_clip=tf.placeholder(tf.float32,
                                   [BATCH_SIZE, None, CROP_SIZE, CROP_SIZE, 3],
                                   name='tf_video_clip'))
    g = build_graph(ph)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    llprint("Initializing Variables ... \n")
    sess.run(tf.global_variables_initializer())
    llprint("Done!\n")

    N = len(batcher.data)

    for i in tqdm.tqdm(range(N)):
        step(sess,g,ph,batcher,dir)

def main():
    batcher = FrameBatcher(type=FLAGS.type)
    save_feats(batcher, dir=os.path.join(FLAGS.dir, FLAGS.type))

if __name__ == '__main__':
    main()