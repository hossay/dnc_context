# dnc_context

## Version info
Python : 2.7.12, CUDA : 9.x, CuDNN : 7.0.5


## ENV Variables
```
# CUDA_HOME
/usr/local/cuda

# LD_LIBRARY_PATH
$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64
```

## Dependent Repos

> dnc_context_api : project for demonstration
```
# at path/to/ouside/of/this/project 
git clone https://bitbucket.org/hossay/dnc_context_api.git
```

## Pretrained C3D weights (sports1m)

> https://drive.google.com/open?id=1DeWt6kZB0NxqGYF8NxeQ7OW-HZW2wsa4
```
tar -xvf pretrained.tar.gz --strip 0
```

## Pretrained Model weights (this project; trained from scratch)

> https://drive.google.com/open?id=10BpTspy7F3TsJoZqnEQGRPUKgdJpv8rI
```
tar -xvf ckpt_repos.tar.gz --strip 0
```
## Extracted C3D Features
> https://drive.google.com/open?id=1s3Kk3tWmDBfEd4XKN3Lx0oS3aTHXLpdl
```
tar -xvf c3d_feats.tar.gz --strip 0
```
## Train Mode
```
python main.py --mode=train --logs_dir=./logs/with_context_sports1m --input_size=4096 --feats_home=/mnt/hdd1/Dataset/Dense_VTT/c3d_feats --checkpoint_dir=/mnt/hdd1/ckpt_repos/with_context_dnc_sports1m/step-last --batch_size=1
```

## Evaluation Mode
```
python main.py --mode=eval --logs_dir=./logs/with_context_sports1m --input_size=4096 --feats_home=/mnt/hdd1/Dataset/Dense_VTT/c3d_feats --checkpoint_dir=/mnt/hdd1/ckpt_repos/with_context_dnc_sports1m/step-last --batch_size=1
```
> Resulting JSON files will be stored at ./results_with_context/*
```
# specify context model type & step to eval   
python eval_scores.py --type=with_context --step=40036
```
> Above command will compute BLEU,METEOR,ROUGE,CIDEr scores automatically


## Web Demo (not realtime)
```
python main.py --mode=demo --logs_dir=./logs/with_context_sports1m --input_size=4096 --feats_home=/mnt/hdd1/Dataset/Dense_VTT/c3d_feats --checkpoint_dir=/mnt/hdd1/ckpt_repos/with_context_dnc_sports1m/step-last --batch_size=1
```
- Resulting JSON files will be stored at ./demo_with_context
- This JSON files contain meta data of each video dataset for demo
- Using ajax API, contents of JSON are parsed into browser(fierefox, ~~safari, chrome~~)

```
# copy resulting JSON file to dnc_contex_api
cp demo_with_context/* /path/to/dnc_context_api/templates
cd /path/to/dnc_context_api
    
# open the html file using browsers (only firefox)
templates/web_demo_offline.html

# (Optional) If you want to have your own webserver, 
# try implement flaskserver which renders this html file(easy!)
# After implementation, just run
python your_flask_app.py
```

## LiveCam Demo

```
# go to dnc_context_api
cd /path/to/dnc_context_api

# run tf_run_wrapper.py
python tf_run_wrapper.py
```
    