import os
import matplotlib.pyplot as plt
from config import FLAGS

def get_info(data):
    with open(data) as f:
        lines = f.readlines()
    steps, losses, scores = zip(*map(lambda x: x.strip().split('\t'), lines))

    steps = map(lambda x: eval(x), steps)
    losses = map(lambda x: eval(x), losses)
    scores = map(lambda x: 100 * eval(x), scores)

    return steps, losses, scores

def visualize_logs(train, valid):
    if os.path.exists(train) and os.path.exists(valid):
        train_steps, train_losses, train_scores = get_info(train)
        valid_steps, valid_losses, valid_scores = get_info(valid)

        fig, ax1 = plt.subplots()
        h1, = ax1.plot(train_steps, train_losses, color='darkgreen', label='Loss_train')
        h2, = ax1.plot(valid_steps, valid_losses, color='tomato', label='Loss_valid')
        ax1.set_xlabel('Iterations')
        ax1.set_ylabel('Loss')
        ax1.tick_params('y')

        ax2 = ax1.twinx()
        h3, = ax2.plot(train_steps, train_scores, color='darkblue', label='BLEU_train')
        h4, = ax2.plot(valid_steps, valid_scores, color='maroon', label='BLEU_valid')
        ax2.set_ylabel('BLEU-4')
        ax2.tick_params('y')
        plt.legend(handles=[h1,h2,h3,h4], ncol=2,
                   loc='upper center', bbox_to_anchor=(0.5, 1.05),
                   fancybox = True, shadow = True)

        fig.tight_layout()

        plt.show()

# DNC
visualize_logs(os.path.join(FLAGS.logs_dir,'DNC_train.txt'),
               os.path.join(FLAGS.logs_dir,'DNC_val.txt'))

# HRED
visualize_logs(os.path.join(FLAGS.logs_dir,'HRED_train.txt'),
               os.path.join(FLAGS.logs_dir,'HRED_val.txt'))
